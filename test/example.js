var test = require('unit.js');
//var test = unitjs 

describe('Learning by the example', function(){
    it('example variable', function(){
        // just for example of tested value
        var example = 'hello world';
        
        test
            .string(example)
              .startsWith('hello')
              .match(/[a-z]/)
            .given(example = 'you are welcome')
              .string(example)
                .endsWith('welcome')
            
            .when('"example" becomes an object', function(){
                example = {
                  message: 'hello world',
                  name: 'Nico',
                  job: 'developer',
                  from: 'France'
                };
            })
            .then('test the "example" object', function(){
                test.object(example)
                    .hasValue('developer')
                    .hasProperty('name')
                    .hasProperty('from', 'France')
                    .contains({message: 'hello world'})
            })
          
    })
})