var elasticsearch = require('elasticsearch');
var client;
var config
var model_maps = {}

var default_mapping ={ 
    "_default_":{
        "_timestamp" : {
            "enabled" : true,
            "store" : true,
            "format": 'yyyy-MM-dd HH:mm:ss'
        },
        "dynamic" : "strict"
    }
};

var _setClient = function(_config){
    // set config
    config = _config
    
    client = new elasticsearch.Client({
        host: config.host+":"+config.port,
        log: 'trace'
    });
}

exports.init = function(_model_maps, config) {
    model_maps = _model_maps;
    _setClient(config);
    el.create()
};


var checker = function (type, callback) {
    client.indices.existsType({
        index: config.indexName,
        type: type
    }, function (error, exists) {
        callback(error, exists);
    });
};

var createLog = function (type, data, callback) {
    client.indices.getMapping({
        index: config.indexName,
        type: type
    }, function (err, res) {
        var obj = res[config.indexName]["mappings"][type]['properties'];
        var err = [];
        var mapdata = {};
        for (var o in obj) {
            if (!data.hasOwnProperty(o)) {
                err.push(o);
            } else {
                mapdata[o] = data[o];
            }
        }
        if (err.length === 0) {
            client.create({
                index: config.indexName,
                type: type,
                body: mapdata
            }, function (err, resp) {
                if (err) {
                    console.log(err);
                    callback({error: err});
                } else {
                    console.log(resp);
                    callback(resp);
                }
            });

        } else {
            callback({error: err});
        }
    });
}

exports.logit = function( config, type, data ) {
    _setClient(config);
    
    checker(type, function (err, exists) {
        if (exists===true) {
            createLog(type, data, function (res) {
                if (res.hasOwnProperty('error')) {
                    console.log("The following field is not present in the Map [" + type + "]:  " + res.error.toString() + " \nPlease check your data.");
                } else {
                    console.log("Your data has been stored.");
                }
            });
        }else{
            console.log('Type [ '+ type +' ] does not exist!');
        }
    });
};


var el = {
    create: function(stat) {
        client.indices.exists({
            index: config.indexName
        }, function(err, resp){
            if(stat === true){
                el.delete(function(err,resp){
                    //Generate the Models
                    client.indices.create({
                        index: config.indexName,
                        body: {
                        "settings" : {
                            "number_of_shards" : 1
                         },
                        "mappings" : mappings
                        }
                    }, function(err, resp){
                        if(resp){
                            console.log("----->>>CREATE NEW AGAIN");
                            console.log(resp);
                        }else{
                            console.log(err);
                        }
                        process.exit();
                    });                     
                });
            }else if(resp===false){
                //Generate the Models
                client.indices.create({
                    index: config.indexName,
                    body: {
                    "settings" : {
                        "number_of_shards" : 1
                     },
                    "mappings" : mappings
                    }
                }, function(err, resp){
                    if(resp){
                        console.log("----->>>CREATE ONCE");
                        console.log(resp);
                    }else{
                        console.log(err);
                    }
                    process.exit();
                }); 
            }else if(resp === true){
                el.update_map_types();
            }
        });
    },
    update_map_types: function(){
        for(m in model_maps){
            var obj = {}
            obj[m] = model_maps[m];
            var map_individual = extend({}, obj , default_mapping);
            console.log(map_individual);
            client.indices.putMapping({
                index: config.indexName,
                type: m,
                body: map_individual
            },function(err, resp){
                    if(resp){
                        console.log("----->>>MAP TYPES CHECKING AND UPDATE");
                        console.log(resp);
                    }else{
                        console.log(err);
                    }
                    process.exit();
            });
        }
    },
    delete: function(callback){
        //Generate the Models
        client.indices.delete({
            index: config.indexName
        }, function(err, resp){
            callback(resp,err);
        });         
    },
    create_always: function(){
        console.log("----->>>RESETTING");
        this.create(true);
    }
};

function extend(target) {
    var sources = [].slice.call(arguments, 1);
    sources.forEach(function (source) {
        for (var prop in source) {
            target[prop] = source[prop];
        }
    });
    return target;
}